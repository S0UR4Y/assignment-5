//Author: Thiha Min Thein

package assignment5;

import org.junit.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class SphereTest {
    @Test
    public void testGetRadius(){
        double radius= 10;
        Sphere test = new Sphere(radius);
        assertEquals( radius, test.getRadius(), 0.01);
    }
    @Test
    public void testVolume(){
        double radius = 12;
        Sphere test = new Sphere(radius);
        assertEquals(Math.PI* radius * radius * radius, test.getVolume(),0.01);
    }
    @Test
    public void testSurfaceArea(){
        double radius = 21;
        Sphere test = new Sphere(radius);
        assertEquals(4 * Math.PI * radius * radius, test.getSurfaceArea(),0.01);
    }
    @Test   
    public void testConstructor(){
        double radius = -10;
        try{
            Sphere test = new Sphere(radius);
            fail("Test did not throw an exception!");
        } catch (IllegalArgumentException e) {
            System.out.println("Success!");
        }
    }
}
