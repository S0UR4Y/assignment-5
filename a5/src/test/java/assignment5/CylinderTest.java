// Author: Rayane Knadry
package assignment5;

import org.junit.Test;
import static org.junit.Assert.*;



public class CylinderTest {
    @Test
    public void testGetHeight(){
        double height = 10;
        double radius = 12;
        Cylinder test = new Cylinder(height, radius);
        assertEquals(height, test.getHeight(),0.01);
    }
    @Test
    public void testGetRadius(){
        double height = 10;
        double radius = 12;
        Cylinder test = new Cylinder(height, radius);
        assertEquals(radius, test.getRadius(),0.01);
    }
    @Test
    public void testVolume() {
        double height = 10;
        double radius = 12;
        Cylinder test = new Cylinder(height, radius);
        assertEquals(Math.PI * (radius * radius) * height, test.getVolume(),0.01);
    }

    @Test
    public void testArea() {
        double height = 10;
        double radius = 12;
        Cylinder test = new Cylinder(height, radius);
        assertEquals((2 * Math.PI * radius * radius) + (2 * Math.PI * radius * height), test.getSurfaceArea(),0.01);
    }

    @Test
    public void testConstructor() {
        try {
            Cylinder test = new Cylinder(-2, 2);
              Cylinder test2 = new Cylinder(2, -2);
            fail("Test did not throw an exception");
        } catch (IllegalArgumentException e) {
            System.out.println("success");
        }
    }
}
