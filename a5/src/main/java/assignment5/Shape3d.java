package assignment5;


public interface Shape3d {
    double getVolume();
    double getSurfaceArea();

}
