package assignment5;

/**
 * This class purpose is to have a Cylinder 3d shape,
 * that uses the interface Shape3d. This class will be 
 * able to return the volume and the surface area.
 */
public class Cylinder implements Shape3d{
    private double height;
    private double radius;
    /**
     * This Cylinder constructor initializes a Cylinder with the given
     * parameters, height and radius.
     * @return Nothing is returned
     */
    public Cylinder(double height, double radius){
        if (height < 0) {
            throw new IllegalArgumentException("Height cannot be negative");
        } else if (radius < 0){
            throw new IllegalArgumentException("Radius cannot be negative!");
        }
        this.height = height;
        this.radius = radius;
    }

    /**
     * This getter method returns the height
     * @return This returns the height (double)
     */
    public double getHeight(){
        return this.height;
    }

    /**
     * This getter method returns the radius
     * @return This returns the radius (double)
     */

    public double getRadius(){
        return this.radius;
    }

    /** 
     * This method calulates the volume.
     * @return The volume of the Cylinder
     * @throws UnsupportedOperationException is to prevent the methods
     * to have an error when there is nothing returned.
     * 
     */
    @Override 
    public double getVolume(){
        return Math.PI * this.radius * this.radius * this.height;
    }

     /** 
     * This method calulates the volume.
     * @return The surface area of the Cylinder
     * @throws UnsupportedOperationException is to prevent the methods
     * to have an error when there is nothing returned.
     * 
     */
    @Override
    public double getSurfaceArea(){
        return 2 * Math.PI * this.radius * this.radius + 2 * Math.PI * this.radius * this.height;
    }
}