package assignment5;

/**
 * This class represents a 3D sphere and implements the Shape3d interface.
 * It provides methods to calculate the volume and surface area of the sphere.
 */
public class Sphere implements Shape3d {
    private double radius;
    /**
     * returns radius
     * @return this.radius
     */
    public double getRadius(){
        return this.radius;
    }
    /**
     * initilises Sphere with filed radius
     * @param radius
     */
    public Sphere(double radius){
        if (radius<0)
        throw new IllegalArgumentException("radius is invalid"
        );
        this.radius=radius;
    }
    /**
     * Calculates and returns the volume of the sphere.
     * @return The volume of the sphere.
     */
    @Override
    public double getVolume() {
        return Math.PI*(this.radius*this.radius*this.radius);
    }

    /**
     * Calculates and returns the surface area of the sphere.
     * @return The surface area of the sphere.
     */
    @Override
    public double getSurfaceArea() {
        return 4*Math.PI*(this.radius*this.radius);
    }
}
